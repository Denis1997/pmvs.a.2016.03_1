package com.example.shilo.pmvsa2016shilo03;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick1(View view)
    {
        startActivity(new Intent(this, secondActivity.class));
    }

    public void onClick2(View view)
    {
        startActivity(new Intent(this, thirdActivity.class));
    }

    public void onClick3(View view)
    {
        startActivity(new Intent(this, fourthActivity.class));
    }

    public void onClick4(View view)
    {
        this.finish();
    }
}
