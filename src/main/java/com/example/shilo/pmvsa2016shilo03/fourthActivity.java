package com.example.shilo.pmvsa2016shilo03;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class fourthActivity extends AppCompatActivity {

    private Button buttonBg;
    private boolean isPressed = false;
    private int nonPressedColor;
    private int pressedColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);
        buttonBg = (Button)findViewById(R.id.handsomeButton);
        nonPressedColor = getResources().getColor(R.color.colorButton);
        pressedColor = getResources().getColor(R.color.colorPressedButton);
    }

    public void onButtonClick(View view)
    {
        isPressed = !isPressed;
        GradientDrawable gd = ((GradientDrawable)view.getBackground());
        gd.setColor(isPressed ? pressedColor : nonPressedColor);
        buttonBg.setText(buttonBg.getText());
    }
}
